use std::collections::HashMap;

use rusqlite::{types::ToSql, Connection, Row, NO_PARAMS};

pub const PK: &str = "PRIMARY KEY";
pub const INTEGER_PK: &str = "INTEGER PRIMARY KEY";

pub struct Field {
    name: String,
    field_type: String,
}

impl Field {
    pub fn new(name: &str, field_type: &str) -> Field {
        Field {
            name: String::from(name),
            field_type: String::from(field_type),
        }
    }
}

pub trait Model {
    fn fields() -> Vec<Field>;
    fn from_values(values: &Row) -> Self;
    fn table_name() -> &'static str;
    fn values(&self) -> HashMap<String, &ToSql>;
}

pub struct DB {
    path: String,
}

impl DB {
    pub fn new(path: &str) -> DB {
        DB {
            path: String::from(path),
        }
    }

    pub fn create_table<T: Model>(&self) {
        let create = creation_sql::<T>();
        self.make_query(&create, NO_PARAMS).unwrap_or_else(|error| {
            if let rusqlite::Error::SqliteFailure(_, description) = &error {
                if let Some(text) = &description {
                    if text == &format!("table {} already exists", T::table_name()) {
                        // This is an expected error, we don't have to do anything
                    } else {
                        panic!("{:?}", error);
                    }
                } else {
                    panic!("{:?}", error);
                }
            } else {
                panic!("{:?}", error);
            }
        });
    }

    pub fn insert_one<T: Model>(&self, row: &T) {
        let mut column_description = String::new();
        let mut values_description = String::new();
        let mut values_vector = Vec::new();
        let values = row.values();
        let fields = T::fields();
        let mut index = 0;
        for field in fields.iter() {
            let name = &field.name;
            if field.field_type.contains(PK) {
                // Let SQL add the primary key
                continue;
            }
            index += 1;
            column_description.push_str(&format!("{},", name));
            values_description.push_str(&format!("?{},", index));
            values_vector.push(&*values[name])
        }
        // Remove the last, extraneous commas
        column_description.pop();
        values_description.pop();
        let insertion_sql = format!(
            "INSERT INTO {} ({}) VALUES ({});",
            T::table_name(),
            column_description,
            values_description
        );
        self.make_query(&insertion_sql, values_vector).unwrap();
    }

    pub fn all<T: Model>(&self) -> Result<Vec<T>, rusqlite::Error> {
        // A shortcut to calling select with an empty WHERE clause
        self.select::<T, &[&ToSql]>("", NO_PARAMS)
    }

    // TODO: figure out how to not have to specify P every time
    pub fn select<T: Model, P>(
        &self,
        where_clause: &str,
        params: P,
    ) -> Result<Vec<T>, rusqlite::Error>
    where
        P: IntoIterator,
        P::Item: ToSql,
    {
        let query = format!("SELECT * FROM {} {};", T::table_name(), where_clause);
        let conn = Connection::open(&self.path).unwrap();
        let mut statement = conn.prepare(&query)?;
        let results = statement.query_map(params, |row| T::from_values(row))?;
        let mut result_vector = Vec::new();
        for result in results {
            result_vector.push(result?);
        }
        Ok(result_vector)
    }

    fn make_query<P>(&self, query: &str, params: P) -> Result<(), rusqlite::Error>
    where
        P: IntoIterator,
        P::Item: ToSql,
    {
        let conn = Connection::open(&self.path).unwrap();
        conn.execute(query, params)?;
        Ok(())
    }
}

fn creation_sql<T: Model>() -> String {
    let mut table_description = String::new();
    for field in T::fields() {
        let field_description = format!("{} {},", field.name, field.field_type);
        table_description.push_str(&field_description);
    }
    // Remove the last, extraneous comma
    table_description.pop();
    format!("CREATE TABLE {} ({});", T::table_name(), table_description)
}
