use std::{collections::HashMap, env, fmt, io, num::ParseFloatError};

use rusqlite::{types::ToSql, Row};
use time;

mod sql;

fn main() {
    let db_path = get_db_path();
    let db = sql::DB::new(&db_path);
    db.create_table::<Log>();

    let args: Vec<String> = env::args().collect();
    if let Some(val) = args.get(1) {
        if val == "list" {
            list(&db);
        } else {
            eprintln!(
                "Warning: the only allowed command-line argument is \"list\"; you passed \"{}\".",
                val,
            );
            add(&db);
        }
    } else {
        add(&db);
    }
}

fn add(db: &sql::DB) {
    let mut project_raw = String::new();
    println!("Which project?");
    io::stdin().read_line(&mut project_raw).unwrap();
    let project = (&project_raw[0..project_raw.len() - 1]).to_string();

    {
        // Print the most recent log for this project, if any
        let most_recent_where = "WHERE project = ?1 ORDER BY timestamp DESC LIMIT 1";
        let most_recent_params: [&ToSql; 1] = [&project];
        let most_recent_log_list = db
            .select::<Log, &[&ToSql]>(&most_recent_where, &most_recent_params)
            .unwrap();
        let most_recent_log = most_recent_log_list.get(0);
        if let Some(log) = most_recent_log {
            let local_time =
                time::strftime("%a %b %d at %I:%M%P", &time::at(log.timestamp)).unwrap();
            println!("Last log was {}", local_time);
        }
    }

    let parse_float = |raw: &str| -> Result<f64, ParseFloatError> {
        let stripped = &raw[0..raw.len() - 1];
        stripped.parse()
    };
    let hours = prompt_until_valid("How many hours?", "Please enter a number.", parse_float);

    let mut description_raw = String::new();
    println!("Description of work?");
    io::stdin().read_line(&mut description_raw).unwrap();
    let description = (&description_raw[0..description_raw.len() - 1]).to_string();

    let now = time::get_time();
    let new_log = Log {
        id: 0,
        project,
        timestamp: now,
        hours,
        description,
    };
    println!("{}", new_log);
    db.insert_one(&new_log);
}

fn list(db: &sql::DB) {
    let mut where_components = Vec::new();
    let mut params = Vec::<&ToSql>::new();

    let mut project_raw = String::new();
    println!("Which project?");
    io::stdin().read_line(&mut project_raw).unwrap();
    let project = (&project_raw[0..project_raw.len() - 1]).to_string();
    if !project.is_empty() {
        // TODO: build WHERE clause generation into the sql library (like INSERT clause)
        where_components.push(format!("project = ?{}", where_components.len() + 1));
        params.push(&project);
    }

    let parse_date = |raw: &str| -> Result<Option<time::Tm>, time::ParseError> {
        if raw == "\n" {
            Ok(None)
        } else {
            Ok(Some(time::strptime(raw, "%F\n")?))
        }
    };
    let start_date = prompt_until_valid(
        "Start date (YYYY-MM-DD)?",
        "Please enter a valid YYYY-MM-DD date.",
        parse_date,
    );
    let timespec; // to ensure that this lasts as long as params
    if let Some(val) = start_date {
        timespec = val.to_timespec();
        where_components.push(format!("timestamp >= ?{}", where_components.len() + 1));
        params.push(&timespec);
    }

    let mut where_clause;
    if where_components.is_empty() {
        where_clause = String::new();
    } else {
        where_clause = String::from("WHERE ");
        where_clause.push_str(&where_components.join(" AND "));
    }

    let all_logs = db.select::<Log, &[&ToSql]>(&where_clause, &params).unwrap();
    for log in all_logs {
        println!("{}", log);
    }
}

fn get_db_path() -> String {
    let var_name = if cfg!(debug_assertions) {
        "TIMESHEET_FILE_DEBUG"
    } else {
        "TIMESHEET_FILE"
    };
    env::var(var_name).unwrap_or_else(|_| {
        eprintln!("Warning: no file specified; using local database.");
        eprintln!(
            "To specify a database to use, set the {} environment variable.",
            var_name,
        );
        String::from("timesheet.sqlite3")
    })
}

fn prompt_until_valid<T, E, F>(initial_prompt: &str, repeat_prompt: &str, parse_fn: F) -> T
where
    F: Fn(&str) -> Result<T, E>,
{
    let mut raw_input;
    println!("{}", initial_prompt);
    loop {
        raw_input = String::new();
        io::stdin().read_line(&mut raw_input).unwrap();
        match parse_fn(&raw_input) {
            Ok(val) => return val,
            Err(_) => {
                println!("{}", repeat_prompt);
                continue;
            }
        };
    }
}

struct Log {
    id: u32,
    project: String,
    timestamp: time::Timespec,
    hours: f64,
    description: String,
}

impl fmt::Display for Log {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let local_time = time::strftime("%a %b %d", &time::at(self.timestamp)).unwrap();
        write!(
            f,
            "{} hours of work on {} on {}:\n{}",
            self.hours, self.project, local_time, self.description,
        )
    }
}

impl sql::Model for Log {
    fn fields() -> Vec<sql::Field> {
        vec![
            sql::Field::new("id", sql::INTEGER_PK),
            sql::Field::new("project", "TEXT NOT NULL"),
            sql::Field::new("timestamp", "DATETIME NOT NULL"),
            sql::Field::new("hours", "FLOAT NOT NULL"),
            sql::Field::new("description", "TEXT NOT NULL"),
        ]
    }
    fn from_values(values: &Row) -> Log {
        Log {
            id: values.get(0),
            project: values.get(1),
            timestamp: values.get(2),
            hours: values.get(3),
            description: values.get(4),
        }
    }
    fn table_name() -> &'static str {
        "log"
    }
    fn values(&self) -> HashMap<String, &ToSql> {
        let mut ret: HashMap<String, &ToSql> = HashMap::new();
        ret.insert(String::from("id"), &self.id);
        ret.insert(String::from("project"), &self.project);
        ret.insert(String::from("timestamp"), &self.timestamp);
        ret.insert(String::from("hours"), &self.hours);
        ret.insert(String::from("description"), &self.description);
        ret
    }
}
